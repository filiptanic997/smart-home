/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alarmee;

import entities.*;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Filip Tanic
 */
public class AlarmApp {
    
    @Resource(lookup="jms/__defaultConnectionFactory")
    private static ConnectionFactory cf;
    
    /*@Resource(lookup="CommunicationQueue")
    private static javax.jms.Queue comQueue;*/
    
    @Resource(lookup="UserQ")
    private static javax.jms.Queue userQ;
    
    @Resource(lookup="AlarmQ")
    private static javax.jms.Queue alarmQ;
    
    @Resource(lookup="SoundQ")
    private static javax.jms.Queue soundQ;
    
    @Resource(lookup="AlarmReplyQ")
    private static javax.jms.Queue alarmReplyQ;
    
    private static JMSContext context;

    private static AlarmApp app = null;
    
    private EntityManagerFactory emf;
    private EntityManager em;
    
    private boolean stop = false;
    
    static String[] days = {"mon","tue","wed","thu","fri","sat","sun"};
    private static int getIndexOfTag(String tag){
        for(int i = 0; i < 7; i++)
            if(tag.equals(days[i]))
                return i;
        return -1;
    }
    
    private AlarmApp(){
        emf = Persistence.createEntityManagerFactory("AlarmEEPU");
        em = emf.createEntityManager();
    }
    
    public static AlarmApp getAlarmApp(){
        if(app == null)
            app = new AlarmApp();
        
        return app;
    }
    
    //needs an overhaul
    private String getAllAlarms(){
        Query q = em.createNamedQuery("Alarm.findAll");
       
        String toRet = "";
        for (Iterator it = q.getResultList().iterator(); it.hasNext();) {
            Alarm a = (Alarm) it.next();
            
            toRet = toRet + a.getIdAlarm() + ":" + String.format("%02d", a.getHour()) + ":" + String.format("%02d", a.getMinute());
            for(int i = 0; i < 7; i++){
                toRet += ":";
                
                if(a.getDays().charAt(i) == 'y')
                    toRet = toRet + "1";
                else
                    toRet = toRet + "0";
            }
            toRet += "\n";
        }
        if(toRet.equals(""))
            toRet = "-100";
        return toRet;
    }

    private void createNewAlarm(int hour, int minute, String[] args, String idSong){
        Alarm alarm = new Alarm(0, hour, minute, idSong);
        StringBuilder theseDays = new StringBuilder("nnnnnnn");
        for(int i = 2; i < args.length; i++)
            theseDays.setCharAt(Integer.parseInt(args[i]), 'y');
        alarm.setDays(theseDays.toString());
        
        em.getTransaction().begin();
        
        em.persist(alarm);
                        
        em.getTransaction().commit();
    }
    
    private void deleteAlarm(int idAlarm){
        Query q = em.createNamedQuery("Alarm.deleteByIdAlarm");
        q.setParameter("idAlarm", idAlarm);
        em.getTransaction().begin();
                        
        q.executeUpdate();
                        
        em.getTransaction().commit();
    }
    
    private void updateAlarm(int idAlarm, int hour, int minute){
        Query q = em.createNamedQuery("Alarm.findByIdAlarm");
        q.setParameter("idAlarm", idAlarm);
        Object target = q.getSingleResult();
        
        if(target != null){
            Alarm alarm = (Alarm) target;
            em.getTransaction().begin();
        
            if(0 <= hour && hour <= 23 && 0 <= minute && minute <= 59){
                alarm.setHour(hour);
                alarm.setMinute(minute);
            }   
        
            em.getTransaction().commit();
        }
    }
    
    private void updateRingtone(int idAlarm, String newSong){
        Query q = em.createNamedQuery("Alarm.findByIdAlarm");
        q.setParameter("idAlarm", idAlarm);
        Object target = q.getSingleResult();
        
        if(target != null){
            Alarm alarm = (Alarm) target;
            em.getTransaction().begin();
            
            alarm.setIdSong(newSong);
            
            em.getTransaction().commit();
        }
    }
    
    private static void sendReply(JMSProducer sender, String from, String to, Integer userid, String message) throws JMSException{
        TextMessage reply = context.createTextMessage(message);
        reply.setStringProperty("from", from);
        reply.setStringProperty("to", to);
        reply.setIntProperty("userid", userid);
        switch(reply.getStringProperty("to")){
            case "user":
                sender.send(userQ, reply);
                break;
            /*case "sound":
                sender.send(soundQ, reply);
                break;*/
            /*case "planner":
                sender.send(plannerQ, reply);
                break;*/
            default:
                break;
        }
    }
    
    void ringAlarm(Alarm alarm, int today){
        try {
            em.getTransaction().begin();
            
            alarm.deactivateOnDay(today);
            
            em.getTransaction().commit();
            
            TextMessage msg = app.context.createTextMessage(alarm.getIdSong());
            msg.setStringProperty("from", "alarm");
            msg.setIntProperty("userid", -1);
            msg.setStringProperty("to", "sound");
            msg.setStringProperty("op", "play");
            msg.setBooleanProperty("noReply", true);
            
            JMSProducer sender = app.context.createProducer();
            sender.send(soundQ, msg);
            
            System.out.println("Alarm " + alarm.getIdAlarm() + " is going off");
            
        } catch (JMSException ex) {
            Logger.getLogger(AlarmApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        
        AlarmApp a = getAlarmApp();
        a.context = a.cf.createContext();
        
        JMSConsumer receiver = a.context.createConsumer(alarmQ);
        JMSConsumer soundReceiver = a.context.createConsumer(alarmReplyQ);
        JMSProducer sender = a.context.createProducer();
        
        new Ticker().start();
        
        while(true){
            Message receivedMsg = null;
            TextMessage msg = null;
            TextMessage reply = null;
            
            try {
                receivedMsg = receiver.receive();
                
                switch(receivedMsg.getStringProperty("op")){
                    //get all alarms
                    case "getAll":
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            a.getAllAlarms());
                        break;
                        
                    //creating a new alarm
                    case "create":
                        String msgBody = receivedMsg.getBody(String.class);
                        String[] msgContent = msgBody.split("__");
                        String[] rArgs = msgContent[0].split(":");
                        String songName = msgContent[1];
                        for(String s : msgContent)
                            System.out.println(s);
                        
                        msg = a.context.createTextMessage(songName);
                        msg.setStringProperty("from", "alarm");
                        msg.setIntProperty("userid", receivedMsg.getIntProperty("userid"));
                        msg.setStringProperty("to", "sound");
                        msg.setStringProperty("op", "search");
                        msg.setBooleanProperty("searchOnly", true);
                        sender.send(soundQ, msg);
                        
                        //System.out.println("Cekam msg od sounda");
                        
                        reply = (TextMessage) soundReceiver.receive();
                        String songId = reply.getText();
                
                        //System.out.println("Primio sam msg od sounda");
                        
                        if(rArgs.length >= 2 && rArgs.length <= 9){
                            int hour = Integer.parseInt(rArgs[0]);
                            int minute = Integer.parseInt(rArgs[1]);
                            a.createNewAlarm(hour, minute, rArgs, songId);
                            sendReply(sender,receivedMsg.getStringProperty("to"),
                                                receivedMsg.getStringProperty("from"),
                                                receivedMsg.getIntProperty("userid"),
                                                "Success!");
                        }
                        else sendReply(sender,receivedMsg.getStringProperty("to"),
                                                receivedMsg.getStringProperty("from"),
                                                receivedMsg.getIntProperty("userid"),
                                                "Invalid arguments! Usage: hh:mm \\n mon tue...");
                        break;
                    
                    //deleting an existing alarm
                    case "delete":
                        a.deleteAlarm(Integer.parseInt(receivedMsg.getBody(String.class)));
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            "Success!");
                        break;
                    //updating an existing alarm
                    case "updateRingtone":
                        String newRingtoneTitle = receivedMsg.getBody(String.class);
                        
                        msg = a.context.createTextMessage(newRingtoneTitle);
                        msg.setStringProperty("from", "alarm");
                        msg.setIntProperty("userid", receivedMsg.getIntProperty("userid"));
                        msg.setStringProperty("to", "sound");
                        msg.setStringProperty("op", "search");
                        msg.setBooleanProperty("searchOnly", true);
                        sender.send(soundQ, msg);
                        
                        reply = (TextMessage) soundReceiver.receive();
                        String newRingtone = reply.getText();
                        
                        a.updateRingtone(receivedMsg.getIntProperty("idAlarm"), newRingtone);
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            "Success!");
                        break;
                    
                    default:
                        break;
                }
            } catch (JMSException ex) {
                Logger.getLogger(AlarmApp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception e){
                Logger.getLogger(AlarmApp.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        
        
    }
    
}
