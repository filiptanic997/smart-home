/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alarmee;

import java.util.Calendar;
import entities.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Filip Tanic
 */
public class Ticker extends Thread{
    
    Calendar previousDateTime = Calendar.getInstance();
    Calendar currentDateTime;
    
    private EntityManagerFactory emf;
    private EntityManager em;
    
    public Ticker(){
        emf = Persistence.createEntityManagerFactory("AlarmEEPU");
        em = emf.createEntityManager();
    }

    @Override
    public void run() {
        
        System.out.println("Ticker active!");
        
        while(true){
            try {
                currentDateTime = Calendar.getInstance();
                
                int dayOfWeek = currentDateTime.get(Calendar.DAY_OF_WEEK);
                if(dayOfWeek == 1)
                    dayOfWeek = 8;
                dayOfWeek -= 2;
                    
                /*System.out.println("Woke up! Time: " + currentDateTime.get(Calendar.HOUR_OF_DAY) + ":"
                        + currentDateTime.get(Calendar.MINUTE) + " " + AlarmApp.days[dayOfWeek]);*/
                
                if(!(currentDateTime.get(Calendar.HOUR_OF_DAY) == previousDateTime.get(Calendar.HOUR_OF_DAY)
                        && currentDateTime.get(Calendar.MINUTE) == previousDateTime.get(Calendar.MINUTE))){
                    
                    
                    Query q = em.createNamedQuery("Alarm.findAll");
                    List<Alarm> alarms = q.getResultList();
                    
                    for(Alarm alarm : alarms){
                        if(alarm.getHour() == currentDateTime.get(Calendar.HOUR_OF_DAY)
                            && alarm.getMinute() == currentDateTime.get(Calendar.MINUTE)
                            && alarm.isActiveOnDay(dayOfWeek)){//should ring
                            
                            AlarmApp.getAlarmApp().ringAlarm(alarm, dayOfWeek);
                        }
                    }
                }
                
                previousDateTime = currentDateTime;
                sleep(6000);
                
            } catch (InterruptedException ex) {
                Logger.getLogger(Ticker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
