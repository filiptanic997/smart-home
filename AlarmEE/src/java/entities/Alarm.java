/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Filip Tanic
 */
@Entity
@Table(name = "alarm")
@NamedQueries({
    @NamedQuery(name = "Alarm.findAll", query = "SELECT a FROM Alarm a")
    , @NamedQuery(name = "Alarm.findByIdAlarm", query = "SELECT a FROM Alarm a WHERE a.idAlarm = :idAlarm")
    , @NamedQuery(name = "Alarm.findByHour", query = "SELECT a FROM Alarm a WHERE a.hour = :hour")
    , @NamedQuery(name = "Alarm.findByMinute", query = "SELECT a FROM Alarm a WHERE a.minute = :minute")
    , @NamedQuery(name = "Alarm.deleteByIdAlarm", query = "DELETE FROM Alarm a WHERE a.idAlarm = :idAlarm")})
public class Alarm implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id    
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idAlarm")
    private Integer idAlarm;
    @Basic(optional = false)
    @Column(name = "hour")
    private int hour;
    @Basic(optional = false)
    @Column(name = "minute")
    private int minute;
    @Basic(optional = false)
    @Column(name = "days")
    private String days;
    @Basic(optional = false)
    @Column(name = "idSong")
    private String idSong;

    public String getIdSong() {
        return idSong;
    }

    public void setIdSong(String idSong) {
        this.idSong = idSong;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Alarm() {
        days = "nnnnnnn";
    }

    public Alarm(Integer idAlarm) {
        this.idAlarm = idAlarm;
        days = "nnnnnnn";
    }

    public Alarm(Integer idAlarm, int hour, int minute, String idSong) {
        this.idAlarm = idAlarm;
        this.hour = hour;
        this.minute = minute;
        days = "nnnnnnn";
        this.idSong = idSong;
    }

    public Integer getIdAlarm() {
        return idAlarm;
    }

    public void setIdAlarm(Integer idAlarm) {
        this.idAlarm = idAlarm;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }
    
    public boolean isActiveOnDay(int day){
        if(day >= 0 && day <= 6){
            if(days.charAt(day) == 'y')
                return true;
        }
        
        return false;
    }
    
    public void deactivateOnDay(int day){
        if(day >= 0 && day <= 6){
            StringBuilder sb = new StringBuilder(days);
            sb.setCharAt(day, 'n');
            days = sb.toString();
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAlarm != null ? idAlarm.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alarm)) {
            return false;
        }
        Alarm other = (Alarm) object;
        if ((this.idAlarm == null && other.idAlarm != null) || (this.idAlarm != null && !this.idAlarm.equals(other.idAlarm))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Alarm[ idAlarm=" + idAlarm + " ]";
    }
    
}