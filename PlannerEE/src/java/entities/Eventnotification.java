/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Filip Tanic
 */
@Entity
@Table(name = "eventnotification")
@NamedQueries({
    @NamedQuery(name = "Eventnotification.findAll", query = "SELECT e FROM Eventnotification e")
    , @NamedQuery(name = "Eventnotification.findByIdEventNotification", query = "SELECT e FROM Eventnotification e WHERE e.idEventNotification = :idEventNotification")
    , @NamedQuery(name = "Eventnotification.findByDatetime", query = "SELECT e FROM Eventnotification e WHERE e.datetime = :datetime")
    , @NamedQuery(name = "Eventnotification.findByIdEvent", query = "SELECT e FROM Eventnotification e WHERE e.idEvent = :idEvent")
    , @NamedQuery(name = "Eventnotification.deleteByIdEvent", query = "DELETE FROM Eventnotification e WHERE e.idEvent = :idEvent")})
public class Eventnotification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEventNotification")
    private Integer idEventNotification;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar datetime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idEvent")
    private int idEvent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idSong")
    private String idSong;

    public Eventnotification() {
    }

    public Eventnotification(Integer idEventNotification) {
        this.idEventNotification = idEventNotification;
    }

    public Eventnotification(Integer idEventNotification, Calendar datetime, int idEvent, String idSong) {
        this.idEventNotification = idEventNotification;
        this.datetime = datetime;
        this.idEvent = idEvent;
        this.idSong = idSong;
    }

    public Integer getIdEventNotification() {
        return idEventNotification;
    }

    public void setIdEventNotification(Integer idEventNotification) {
        this.idEventNotification = idEventNotification;
    }

    public Calendar getDatetime() {
        return datetime;
    }

    public void setDatetime(Calendar datetime) {
        this.datetime = datetime;
    }

    public int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public String getIdSong() {
        return idSong;
    }

    public void setIdSong(String idSong) {
        this.idSong = idSong;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEventNotification != null ? idEventNotification.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventnotification)) {
            return false;
        }
        Eventnotification other = (Eventnotification) object;
        if ((this.idEventNotification == null && other.idEventNotification != null) || (this.idEventNotification != null && !this.idEventNotification.equals(other.idEventNotification))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Eventnotification[ idEventNotification=" + idEventNotification + " ]";
    }
    
}
