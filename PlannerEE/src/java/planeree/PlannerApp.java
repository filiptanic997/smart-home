/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planeree;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import entities.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.Persistence;

/**
 *
 * @author Filip Tanic
 */
public class PlannerApp {

    @Resource(lookup="jms/__defaultConnectionFactory")
    private static ConnectionFactory cf;
    
    /*@Resource(lookup="CommunicationQueue")
    private static javax.jms.Queue comQueue;*/
    
    @Resource(lookup="UserQ")
    private static javax.jms.Queue userQ;
    
    @Resource(lookup="SoundQ")
    private static javax.jms.Queue soundQ;
    
    @Resource(lookup="PlannerQ")
    private static javax.jms.Queue plannerQ;
    
    @Resource(lookup="PlannerReplyQ")
    private static javax.jms.Queue plannerReplyQ;
    
    private static JMSContext context;
    
    private static EntityManagerFactory emf;
    private static EntityManager em;
    
    private static String getAllEvents(){
        Query q = em.createNamedQuery("Event.findAll");
       
        String toRet = "";
        for (Iterator it = q.getResultList().iterator(); it.hasNext();) {
            Event e = (Event) it.next();
            Calendar c = e.getDatetime();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            int day = c.get(Calendar.DAY_OF_MONTH);
            int month = c.get(Calendar.MONTH);
            int year = c.get(Calendar.YEAR);
            
            toRet = toRet + e.getIdEvent() + "__" + e.getDestination() + "__" + hour + "__" + minute + "__" + day + "__" + month + "__" + year
                    + "__" + e.getDescription();
            
            toRet += "\n";
        }
        
        if(toRet.equals(""))
            toRet = "-100";
        
        return toRet;
    }
    
    private static int createNewEvent(String descr, String dest, int hour, int minute, int day, int month, int year){
        Date d = new Date(year - 1900, month - 1, day, hour, minute);
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        Event event = new Event(1, c, descr, dest);
        
        em.getTransaction().begin();
        
        em.persist(event);
                        
        em.getTransaction().commit();
        
        System.out.println("Event with id " + event.getIdEvent() + " created!");
        
        return event.getIdEvent();
    }
    
    private static void deleteEvent(Integer idEvent){
        Query qe = em.createNamedQuery("Event.deleteByIdEvent");
        Query qen = em.createNamedQuery("Eventnotification.deleteByIdEvent");
        qe.setParameter("idEvent", idEvent);
        qen.setParameter("idEvent", idEvent);
        em.getTransaction().begin();
                        
        qe.executeUpdate();
        qen.executeUpdate();
                        
        em.getTransaction().commit();
    }
    
    private static void createNewEventNotification(int idEvent, String idSong, int hour, int minute, int day, int month, int year, int minutesOffset){
        Date d = new Date(year - 1900, month - 1, day, hour, minute);
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.MINUTE, -1 - minutesOffset);
        Eventnotification notif = new Eventnotification(1, c, idEvent, idSong);
        
        em.getTransaction().begin();
        
        em.persist(notif);
                        
        em.getTransaction().commit();
        
        System.out.println("Eventnotification with id " + notif.getIdEvent() + " created!");
    }
    
    static void ringNotification(Eventnotification en){
        try {
            TextMessage msg = context.createTextMessage(en.getIdSong());
            msg.setStringProperty("from", "planner");
            msg.setIntProperty("userid", -1);
            msg.setStringProperty("to", "sound");
            msg.setStringProperty("op", "play");
            msg.setBooleanProperty("noReply", true);
            
            JMSProducer sender = context.createProducer();
            sender.send(soundQ, msg);
            
            Query q = em.createNamedQuery("Eventnotification.deleteByIdEvent");
            q.setParameter("idEvent", en.getIdEvent());
            
            em.getTransaction().begin();
            
            q.executeUpdate();
            
            em.getTransaction().commit();
            
            System.out.println("Event " + en.getIdEvent() + " is going off");
            
        } catch (JMSException ex) {
            Logger.getLogger(PlannerApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void sendReply(JMSProducer sender, String from, String to, Integer userid, String message) throws JMSException{
        TextMessage reply = context.createTextMessage(message);
        reply.setStringProperty("from", from);
        reply.setStringProperty("to", to);
        reply.setIntProperty("userid", userid);
        switch(reply.getStringProperty("to")){
            case "user":
                sender.send(userQ, reply);
                break;
            /*case "sound":
                sender.send(soundQ, reply);
                break;*/
            default:
                break;
        }
    }
    
    public static void main(String[] args) {
        
        emf = Persistence.createEntityManagerFactory("PlannerEEPU");
        em = emf.createEntityManager();
        
        context = cf.createContext();
        
        JMSConsumer receiver = context.createConsumer(plannerQ);
        JMSConsumer soundReceiver = context.createConsumer(plannerReplyQ);
        JMSProducer sender = context.createProducer();
        
        new Ticker().start();
        
        while(true){
            Message receivedMsg = null;
            TextMessage msg = null;
            TextMessage reply = null;
            
            try {
                receivedMsg = receiver.receive();
                
                switch(receivedMsg.getStringProperty("op")){
                    //get all events
                    case "getAll":
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            getAllEvents());
                        break;
                        
                    //creating a new event
                    case "create":
                        String msgBody = receivedMsg.getBody(String.class);
                        String[] msgContent = msgBody.split("__");
                        
                        String descr = msgContent[0];
                        int hour = Integer.parseInt(msgContent[1]);
                        int minute = Integer.parseInt(msgContent[2]);
                        int day = Integer.parseInt(msgContent[3]);
                        int month = Integer.parseInt(msgContent[4]);
                        int year = Integer.parseInt(msgContent[5]);
                        int idEvent = -1;
                         
                        String destination = null;
                        int minutesOffset = 0;
                        if(receivedMsg.getBooleanProperty("dest")){
                            Process newProcess = Runtime.getRuntime().exec("java -jar \".\\..\\Resources\\google-api-client-1.0-SNAPSHOT-jar-with-dependencies.jar\" \""
                                    + "Bulevar Kralja Aleksandra 73" + "\" \"" + msgContent[6] + "\" driving");
                            BufferedReader procOutput = new BufferedReader(new InputStreamReader(newProcess.getInputStream()));
                            
                            destination = msgContent[6];
                            String distance = procOutput.readLine();
                            int hours = 0;
                            int minutes = 0;
                            if(distance.contains("h")){
                                Pattern p = Pattern.compile("^\\s*([0-9]+)\\s+h\\s+([0-9]+).*$");
                                Matcher m = p.matcher(distance);
                                if(m.find()){
                                    System.out.println("Macthed both! " + m.group(1) + " " + m.group(2));
                                    hours = Integer.parseInt(m.group(1));
                                    minutes = Integer.parseInt(m.group(2));
                                }
                            }
                            else{
                                Pattern p = Pattern.compile("^\\s*([0-9]+).*$");
                                Matcher m = p.matcher(distance);
                                if(m.find()){
                                    System.out.println("Macthed minutes! " + m.group(1));
                                    minutes = Integer.parseInt(m.group(1));
                                }
                            }
                            minutesOffset = hours * 60 + minutes;
                        }
                        try{
                            idEvent = createNewEvent(descr, destination, hour, minute, day, month, year);
                        }
                        catch(DateTimeException e){
                            sendReply(sender,receivedMsg.getStringProperty("to"),
                                                receivedMsg.getStringProperty("from"),
                                                receivedMsg.getIntProperty("userid"),
                                                "Invalid arguments! Date parameters out of range...");
                            break;
                        }
                        
                        if(receivedMsg.getBooleanProperty("notif")){
                            String songName = null;
                            if(receivedMsg.getBooleanProperty("dest"))
                                songName = msgContent[7];
                            else
                                songName = msgContent[6];
                            msg = context.createTextMessage(songName);
                            msg.setStringProperty("from", "planner");
                            msg.setIntProperty("userid", receivedMsg.getIntProperty("userid"));
                            msg.setStringProperty("to", "sound");
                            msg.setStringProperty("op", "search");
                            msg.setBooleanProperty("searchOnly", true);
                            sender.send(soundQ, msg);
                            
                            reply = (TextMessage) soundReceiver.receive();
                            String idSong = reply.getText();
                            System.out.println(minutesOffset);
                            createNewEventNotification(idEvent, idSong, hour, minute, day, month, year, minutesOffset);
                        }

                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            "Success!");
                        break;
                    
                    case "delete":
                        deleteEvent(Integer.parseInt(receivedMsg.getBody(String.class)));
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            "Success!");
                        break;
                        
                    case "update":
                        String[] splitUpdate = receivedMsg.getBody(String.class).split("__");
                        Integer targetEvent = Integer.parseInt(splitUpdate[0]);
                        Query getTarget = em.createNamedQuery("Event.findByIdEvent");
                        getTarget.setParameter("idEvent", targetEvent);
                        Event target = (Event) getTarget.getSingleResult();//LOOPHOLE? Check if null always
                        
                        em.detach(target);
                        
                        if(receivedMsg.getBooleanProperty("uDescr")){
                            target.setDescription(splitUpdate[1]);
                        }
                        if(receivedMsg.getBooleanProperty("uTime")){
                            int newHour = Integer.parseInt(splitUpdate[2]);
                            int newMinute = Integer.parseInt(splitUpdate[3]);
                            int newDay = Integer.parseInt(splitUpdate[4]);
                            int newMonth = Integer.parseInt(splitUpdate[5]);
                            int newYear = Integer.parseInt(splitUpdate[6]);
                            
                            Date d = new Date(newYear - 1900, newMonth - 1, newDay, newHour, newMinute);
                            Calendar c = Calendar.getInstance();
                            c.setTime(d);
                            
                            target.setDatetime(c);
                        }
                        if(receivedMsg.getBooleanProperty("uDest")){
                            target.setDestination(splitUpdate[7]);
                        }
                        
                        em.getTransaction().begin();
                        
                        em.merge(target);
                        if(receivedMsg.getBooleanProperty("uTime") || receivedMsg.getBooleanProperty("uDest")){
                            Query updateEn = em.createNamedQuery("Eventnotification.deleteByIdEvent");
                            updateEn.setParameter("idEvent", target.getIdEvent());
                            updateEn.executeUpdate();
                            
                            //em.getTransaction().commit();
                            //createNewEventNotification(targetEvent, descr, 0, 0, 0, 0, 0, 0);//add a new notification
                        }
                        
                        em.getTransaction().commit();
                        
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            "Success!");
                        break;
                        
                    case "dist":
                        msgBody = receivedMsg.getBody(String.class);
                        String[] msgLocations = msgBody.split("__");
                        Process newProcess = Runtime.getRuntime().exec("java -jar \".\\..\\Resources\\google-api-client-1.0-SNAPSHOT-jar-with-dependencies.jar\" \""
                                    + msgLocations[0] + "\" \"" + msgLocations[1] + "\" driving");
                        
                        String distance = new BufferedReader(new InputStreamReader(newProcess.getInputStream())).readLine();
                        int hours = 0;
                        int minutes = 0;
                        if(distance.contains("h")){
                            Pattern p = Pattern.compile("^\\s*([0-9]+)\\s+h\\s+([0-9]+).*$");
                            Matcher m = p.matcher(distance);
                            if(m.find()){
                                System.out.println("Macthed both! " + m.group(1) + " " + m.group(2));
                                hours = Integer.parseInt(m.group(1));
                                minutes = Integer.parseInt(m.group(2));
                            }
                        }
                        else{
                            Pattern p = Pattern.compile("^\\s*([0-9]+).*$");
                            Matcher m = p.matcher(distance);
                            if(m.find()){
                                System.out.println("Macthed minutes! " + m.group(1));
                                minutes = Integer.parseInt(m.group(1));
                            }
                        }
                        
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            hours + "__" + minutes);
                        break;
                    
                    default:
                        
                        break;
                }
                        
            } catch (JMSException ex) {
                Logger.getLogger(PlannerApp.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PlannerApp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
