/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planeree;

import static java.lang.Thread.sleep;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import entities.*;

/**
 *
 * @author Filip Tanic
 */
public class Ticker extends Thread {
    
    Calendar previousDateTime = Calendar.getInstance();
    Calendar currentDateTime;
    
    private EntityManagerFactory emf;
    private EntityManager em;
    
    public Ticker(){
        emf = Persistence.createEntityManagerFactory("PlannerEEPU");
        em = emf.createEntityManager();
    }

    @Override
    public void run() {
        
        System.out.println("Ticker active!");
        
        while(true){
            try {
                currentDateTime = Calendar.getInstance();
                                                
                if(!(currentDateTime.get(Calendar.HOUR_OF_DAY) == previousDateTime.get(Calendar.HOUR_OF_DAY)
                        && currentDateTime.get(Calendar.MINUTE) == previousDateTime.get(Calendar.MINUTE))){
                    
                    Query q = em.createNamedQuery("Eventnotification.findAll");
                    List<Eventnotification> notifs = q.getResultList();
                    
                    for(Eventnotification notif : notifs){
                        System.out.println(currentDateTime.getTime() + "     " + notif.getDatetime().getTime());
                        if(currentDateTime.getTime().compareTo(notif.getDatetime().getTime()) >= 0){//should ring
                            
                            PlannerApp.ringNotification(notif);
                        }
                    }
                }
                
                previousDateTime = currentDateTime;
                sleep(5000);
                
            } catch (InterruptedException ex) {
                Logger.getLogger(Ticker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
