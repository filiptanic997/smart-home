/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Filip Tanic
 */
@Entity
@Table(name = "song")
@NamedQueries({
    @NamedQuery(name = "Song.findAll", query = "SELECT s FROM Song s")
    , @NamedQuery(name = "Song.findByIdSong", query = "SELECT s FROM Song s WHERE s.songPK.idSong = :idSong")
    , @NamedQuery(name = "Song.findBySongName", query = "SELECT s FROM Song s WHERE s.songName = :songName")
    , @NamedQuery(name = "Song.findByUserId", query = "SELECT s FROM Song s WHERE s.songPK.userId = :userId")
    , @NamedQuery(name = "Song.findBySongIdAndUserId", query = "SELECT s FROM Song s WHERE s.songPK.userId = :userId AND s.songPK.idSong = :idSong")})
public class Song implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SongPK songPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "songName")
    private String songName;

    public Song() {
    }

    public Song(SongPK songPK) {
        this.songPK = songPK;
    }

    public Song(SongPK songPK, String songName) {
        this.songPK = songPK;
        this.songName = songName;
    }

    public Song(String idSong, int userId) {
        this.songPK = new SongPK(idSong, userId);
    }

    public SongPK getSongPK() {
        return songPK;
    }

    public void setSongPK(SongPK songPK) {
        this.songPK = songPK;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (songPK != null ? songPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Song)) {
            return false;
        }
        Song other = (Song) object;
        if ((this.songPK == null && other.songPK != null) || (this.songPK != null && !this.songPK.equals(other.songPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Song[ songPK=" + songPK + " ]";
    }
    
}
