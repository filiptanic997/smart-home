/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sounddeviceee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;
import entities.*;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Filip Tanic
 */
public class SoundDevice {
    
    @Resource(lookup="jms/__defaultConnectionFactory")
    private static ConnectionFactory cf;
    
    /*@Resource(lookup="CommunicationQueue")
    private static javax.jms.Queue comQueue;*/
    
    @Resource(lookup="UserQ")
    private static javax.jms.Queue userQ;
    
    @Resource(lookup="AlarmReplyQ")
    private static javax.jms.Queue alarmQ;
    
    @Resource(lookup="SoundQ")
    private static javax.jms.Queue soundQ;
    
    @Resource(lookup="PlannerReplyQ")
    private static javax.jms.Queue plannerQ;
    
    private static JMSContext context;
    
    private static EntityManagerFactory emf;
    private static EntityManager em;
    
    private static void sendReply(JMSProducer sender, String from, String to, Integer userid, String message) throws JMSException{
        TextMessage reply = context.createTextMessage(message);
        reply.setStringProperty("from", from);
        reply.setStringProperty("to", to);
        reply.setIntProperty("userid", userid);
        switch(reply.getStringProperty("to")){
            case "user":
                sender.send(userQ, reply);
                break;
            case "alarm":
                sender.send(alarmQ, reply);
                break;
            case "planner":
                //System.out.println("Saljem Planeru");
                sender.send(plannerQ, reply);
                //System.out.println("Poslao Planeru");
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {
        
        emf = Persistence.createEntityManagerFactory("SoundDeviceEEPU");
        em = emf.createEntityManager();
        
        context = cf.createContext();
        JMSConsumer receiver = context.createConsumer(soundQ);
        JMSProducer sender = context.createProducer();
        
        while(true){
            
            try {                
                Message receivedMsg = null;
                TextMessage reply = null;
                
                receivedMsg = receiver.receive();
                
                switch(receivedMsg.getStringProperty("op")){
                    case "search":
                        String song = receivedMsg.getBody(String.class);
                        Process newProcess = Runtime.getRuntime().exec("java -jar \".\\..\\Resources\\samples-0.0.1-SNAPSHOT-jar-with-dependencies.jar\" \"" + song + "\"");
                        BufferedReader searchOutput = new BufferedReader(new InputStreamReader(newProcess.getInputStream()));
                        
                        String bestMatch = null;
                        String bestMatchName = null;
                        String line = searchOutput.readLine();
                        while(line != null){//LOOPHOLE?
                            Pattern p = Pattern.compile("^\\s*Video+\\s+Id([^\\s]+).*$");
                            Matcher m = p.matcher(line);
                            if(m.find()){
                                bestMatch = m.group(1);
                                p = Pattern.compile("^\\s*Title:+\\s+([^\\s]+.*[^\\s]+).*$");
                                m = p.matcher(searchOutput.readLine());
                                if(m.find()){
                                    bestMatchName = m.group(1);
                                }
                                break;
                            }
                            line = searchOutput.readLine(); 
                        }
                        System.out.println(bestMatch + " Title: " + bestMatchName);
                        
                        Query exists = em.createNamedQuery("Song.findBySongIdAndUserId");
                        exists.setParameter("idSong", bestMatch);
                        exists.setParameter("userId", receivedMsg.getIntProperty("userid"));
                        Object entity = em.find(Song.class, new SongPK(bestMatch, receivedMsg.getIntProperty("userid")));
                        
                        if(bestMatch != null && entity == null){
                            Song songEnt = new Song(new SongPK(bestMatch, receivedMsg.getIntProperty("userid")), bestMatchName);
                            
        
                            try{
                                em.getTransaction().begin();
                                
                                em.persist(songEnt);
                                
                                em.getTransaction().commit();
                            }
                            catch(Exception e){
                                Logger.getLogger(SoundDevice.class.getName()).log(Level.SEVERE, null, e);
                            }
                            
                        }
                        if(receivedMsg.getBooleanProperty("searchOnly")){//if search only, reply with song id
                            sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            bestMatch);
                            break;
                        }
                        
                        Runtime.getRuntime().exec("\"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe\" " + "https://www.youtube.com/watch?v=" + bestMatch);
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            "Success!");
                        break;
                        
                    case "play":
                        String songId = receivedMsg.getBody(String.class);
                        
                        /*if(songId.length() != 11)
                            sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            "Invalid YouTube video id!");*/
                        
                        Runtime.getRuntime().exec( new String[]{"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe", "https://www.youtube.com/watch?v=" + songId});
                        
                        if(receivedMsg.getBooleanProperty("noReply"))
                            break;
                        
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            "Success!");
                    case "byUser":
                        Integer userId = Integer.parseInt(receivedMsg.getBody(String.class));
                        Query q = em.createNamedQuery("Song.findByUserId");
                        q.setParameter("userId", userId);
                        List<Song> songs = q.getResultList();
                        
                        String toRet = "";
                        for(Song s : songs){
                            toRet = toRet + s.getSongName() + "__";
                        }
                        sendReply(sender,receivedMsg.getStringProperty("to"),
                                            receivedMsg.getStringProperty("from"),
                                            receivedMsg.getIntProperty("userid"),
                                            toRet);
                        break;
                     
                    default:
                        break;
                }
            } catch (IOException ex) {
                Logger.getLogger(SoundDevice.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JMSException ex) {
                Logger.getLogger(SoundDevice.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
