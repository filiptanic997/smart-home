/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userdeviceee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;

/**
 *
 * @author Filip Tanic
 */
public class UserDevice {
    
    private Integer myId;
    
    @Resource(lookup="jms/__defaultConnectionFactory")
    private static ConnectionFactory cf;
    
    @Resource(lookup="UserQ")
    private static javax.jms.Queue userQ;
    
    @Resource(lookup="AlarmQ")
    private static javax.jms.Queue alarmQ;
    
    @Resource(lookup="SoundQ")
    private static javax.jms.Queue soundQ;
    
    @Resource(lookup="PlannerQ")
    private static javax.jms.Queue plannerQ;
    
    private JMSContext context;
    private JMSConsumer consumer;
    /*private JMSConsumer flushAlarmQ;
    private JMSConsumer flushPlannerQ;
    private JMSConsumer flushSoundQ;*/
    private JMSProducer sender;
    
    private boolean end = false;
    
    public UserDevice(Integer id){
        myId = id;
    }
    
    private static String[] days = {"mon","tue","wed","thu","fri","sat","sun"};
    private static int getIndexOfTag(String tag){
        for(int i = 0; i < 7; i++)
            if(tag.equals(days[i]))
                return i;
        return -1;
    }
    
    private static void clrAction() throws IOException{
        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.println("Enter the number to perform action");
    }
    
    private static void clr() throws IOException{
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
    
    private void alarmAction(BufferedReader br) throws IOException, JMSException, InterruptedException{
        clrAction();
        System.out.println("1. Create a new alarm\n"
                            + "2. Delete an existing alarm\n"
                            + "3. Update an alarm ringtone\n"
                            + "0. Back");
        TextMessage msg = null;
        Message reply = null;
        String[] alarms = null;
        HashSet<Integer> alarmIds = null;
        int input = -1;
        
        switch(Integer.parseInt(br.readLine())){
            case 1:
                //clr();
                System.out.print("\nInsert the time in format hh:mm : ");
                String msgBody = "";
                String[] inputHM = br.readLine().split(":");
                if(inputHM.length != 2)
                    break;
                msgBody = msgBody + inputHM[0] + ":" + inputHM[1];
                System.out.print("\nInsert the three-letter day tags on which the alarm should fire: ");
                String[] inputD = br.readLine().split(" ");
                for(String day : inputD)
                    if(getIndexOfTag(day) != -1)
                        msgBody = msgBody + ":" + getIndexOfTag(day);
                System.out.print("\nInsert the name of the ringtone: ");
                String songName = br.readLine();
                msgBody = msgBody + "__" + songName;
                msg = context.createTextMessage(msgBody);
                msg.setStringProperty("from", "user");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("to", "alarm");
                msg.setStringProperty("op", "create");
                
                //System.out.println("Treba da posaljem alarmu");
                sender.send(alarmQ, msg);
                //System.out.println("Poslao sam alarmu");
                
                msg = (TextMessage) consumer.receive();
                System.out.println("\n" + msg.getText() + "\n");
                break;
                
            case 2:
                //clr();
                msg = context.createTextMessage("");
                msg.setStringProperty("from", "user");
                msg.setStringProperty("to", "alarm");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("op", "getAll");
                sender.send(alarmQ, msg);
                
                reply = consumer.receive();
                //clr();
                
                //System.out.println(reply.getBody(String.class));
                alarms = reply.getBody(String.class).split("\n");
                alarmIds = new HashSet();
                if(alarms[0].equals("-100")){
                    System.out.println("There are no alarms!");
                    Thread.sleep(1500);
                    break;
                }
                for(String alarm : alarms){
                    String[] args = alarm.split(":");
                    alarmIds.add(Integer.parseInt(args[0]));
                    System.out.printf("Alarm id: %3s | %2s:%2s", args[0], args[1], args[2]);
                    for(int i = 3; i < 10; i++)
                        System.out.printf(" %3s", args[i].equals("1")? days[i-3] : "/");
                    System.out.print("\n");
                }
                System.out.print("\nInsert the id of the alarm you wish to delete: ");
                input = Integer.parseInt(br.readLine());
                if(alarmIds.contains(input)){
                    msg = context.createTextMessage("" + input);
                    msg.setStringProperty("from", "user");
                    msg.setStringProperty("to", "alarm");
                    msg.setIntProperty("userid", myId);
                    msg.setStringProperty("op", "delete");
                    sender.send(alarmQ, msg);
                    reply = consumer.receive();
                    System.out.println("\n\n" + reply.getBody(String.class) + "\n");
                }
                else{
                    //cls();
                    System.out.println("\n\nNo alarm with such id exists!\n");
                    Thread.sleep(1500);
                }
                break;
                
            case 3:
                //clr();
                msg = context.createTextMessage("");
                msg.setStringProperty("from", "user");
                msg.setStringProperty("to", "alarm");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("op", "getAll");
                sender.send(alarmQ, msg);
                
                reply = consumer.receive();
                //clr();
                
                //System.out.println(reply.getBody(String.class));
                alarms = reply.getBody(String.class).split("\n");
                alarmIds = new HashSet();
                if(alarms[0].equals("-100")){
                    System.out.println("There are no alarms!");
                    Thread.sleep(1500);
                    break;
                }
                for(String alarm : alarms){
                    String[] args = alarm.split(":");
                    alarmIds.add(Integer.parseInt(args[0]));
                    System.out.printf("Alarm id: %3s | %2s:%2s", args[0], args[1], args[2]);
                    for(int i = 3; i < 10; i++)
                        System.out.printf(" %3s", args[i].equals("1")? days[i-3] : "/");
                    System.out.print("\n");
                }
                System.out.print("\nInsert the id of the alarm you wish to update the ringtone of: ");
                input = Integer.parseInt(br.readLine());
                if(alarmIds.contains(input)){
                    System.out.print("\nInsert name of the new ringtone: ");
                    String newRingtone = br.readLine();
                    msg = context.createTextMessage(newRingtone);
                    msg.setStringProperty("from", "user");
                    msg.setStringProperty("to", "alarm");
                    msg.setIntProperty("userid", myId);
                    msg.setStringProperty("op", "updateRingtone");
                    msg.setIntProperty("idAlarm", input);
                    sender.send(alarmQ, msg);
                    //System.out.println("Cekam od alarma");
                    reply = consumer.receive();
                    System.out.println("\n\n" + reply.getBody(String.class) + "\n");
                }
                else{
                    //cls();
                    System.out.println("\n\nNo alarm with such id exists!\n");
                    Thread.sleep(1500);
                }
                
                break;
                
            default:
                break;
        }
    }

    private void soundAction(BufferedReader br) throws IOException, JMSException, InterruptedException{
        clrAction();
        System.out.println("1. Play a song\n"
                            + "2. Show all the songs a user has played\n"
                            + "0. Back");
        TextMessage msg = null;
        Message reply = null;
        switch(Integer.parseInt(br.readLine())){
            case 1:
                //clr();
                System.out.print("\nInsert your search:");
                String toSearch = br.readLine();
                msg = context.createTextMessage(toSearch);
                msg.setStringProperty("from", "user");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("to", "sound");
                msg.setStringProperty("op", "search");
                msg.setBooleanProperty("searchOnly", false);
                sender.send(soundQ, msg);
                
                msg = (TextMessage) consumer.receive();
                System.out.println("\n" + msg.getText() + "\n");
                break;
                
            case 2:
                System.out.print("\nInsert the user's Id:");
                String usersId = br.readLine();
                msg = context.createTextMessage(usersId);
                msg.setStringProperty("from", "user");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("to", "sound");
                msg.setStringProperty("op", "byUser");
                //msg.setBooleanProperty("searchOnly", false);
                sender.send(soundQ, msg);
                
                msg = (TextMessage) consumer.receive();
                System.out.println("\n" + msg.getText() + "\n");
                break;
                
            default:
                break;
        }
    }
    
    private void plannerAction(BufferedReader br) throws IOException, JMSException, InterruptedException{
        clrAction();
        System.out.println("1. Create a new event\n"
                            + "2. Delete an existing event\n"
                            + "3. Update an event\n"
                            + "4. List all events\n"
                            + "5. Travel time between...\n"
                            + "0. Back");
        TextMessage msg = null;
        Message reply = null;
        String[] events = null;
        HashSet<Integer> eventIds = null;
        int input = -1;
        
        switch(Integer.parseInt(br.readLine())){
            case 1:
                //clr();
                System.out.print("\nInsert the description of the event(max 256 characters): ");
                String msgBody = "";
                msgBody = msgBody + br.readLine();
                
                System.out.print("\nInsert the date of the event(hh mm dd MM yyyy): ");
                String[] inputD = br.readLine().split(" ");
                int hour = Integer.parseInt(inputD[0]);
                int minute = Integer.parseInt(inputD[1]);
                int day = Integer.parseInt(inputD[2]);
                int month = Integer.parseInt(inputD[3]);
                int year = Integer.parseInt(inputD[0]);
                if(hour < 0 || hour > 23 || minute < 0 || minute > 59 ||
                    day < 0 || day > 31 || month < 0 || month > 12 || year < 0 || year > 10000){
                    System.out.println("\n\nInvalid date!\n");
                    Thread.sleep(1500);
                    break;
                }
                for(String param : inputD)
                    msgBody = msgBody + "__" + param;
                
                System.out.print("\nDo You wish to enter a location of the event(y or n)? ");
                String enterLocation = br.readLine();
                Boolean dest = false;
                if(enterLocation.equals("y")){
                    System.out.print("\nInsert the name of Your destination: ");
                    enterLocation = br.readLine();
                    msgBody = msgBody + "__" + enterLocation;
                    dest = true;
                }
                
                System.out.print("\nDo You wish to wish to be notified before the event(y or n)? ");
                String setUpAlarm = br.readLine();
                Boolean notif = false;
                if(setUpAlarm.equals("y")){//TO BE IMPLEMENTED...
                    System.out.print("\nInsert the name of the ringtone: ");
                    setUpAlarm = br.readLine();
                    msgBody = msgBody + "__" + setUpAlarm;
                    notif = true;
                }
                
                msg = context.createTextMessage(msgBody);
                msg.setStringProperty("from", "user");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("to", "planner");
                msg.setStringProperty("op", "create");
                msg.setBooleanProperty("notif", notif);
                msg.setBooleanProperty("dest", dest);
                sender.send(plannerQ, msg);
                
                msg = (TextMessage) consumer.receive();
                System.out.println("\n" + msg.getText() + "\n");
                break;
                
            case 2:
                //clr();
                msg = context.createTextMessage("");
                msg.setStringProperty("from", "user");
                msg.setStringProperty("to", "planner");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("op", "getAll");
                sender.send(plannerQ, msg);
                
                reply = consumer.receive();
                //clr();
                
                events = reply.getBody(String.class).split("\n");
                eventIds = new HashSet();
                if(events[0].equals("-100")){
                    System.out.println("There are no events!");
                    Thread.sleep(1500);
                    break;
                }
                for(String event : events){
                    String[] args = event.split("__");
                    eventIds.add(Integer.parseInt(args[0]));
                    System.out.printf("Event id: %3s | %s | %2s:%2s %2s/%2s/%4s | %s", 
                                    args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
                    System.out.print("\n");
                    
                }
                
                System.out.print("\nInsert the id of the event you wish to delete: ");
                input = Integer.parseInt(br.readLine());
                if(eventIds.contains(input)){
                    msg = context.createTextMessage("" + input);
                    msg.setStringProperty("from", "user");
                    msg.setStringProperty("to", "planner");
                    msg.setIntProperty("userid", myId);
                    msg.setStringProperty("op", "delete");
                    sender.send(plannerQ, msg);
                    
                    reply = consumer.receive();
                    System.out.println("\n\n" + reply.getBody(String.class) + "\n");
                }
                else{
                    //cls();
                    System.out.println("\n\nNo event with such id exists!\n");
                    Thread.sleep(1500);
                }
                break;
                
            case 3:
                //clr();
                msg = context.createTextMessage("");
                msg.setStringProperty("from", "user");
                msg.setStringProperty("to", "planner");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("op", "getAll");
                sender.send(plannerQ, msg);
                
                reply = consumer.receive();
                //clr();
                
                events = reply.getBody(String.class).split("\n");
                eventIds = new HashSet();
                if(events[0].equals("-100")){
                    System.out.println("There are no events!");
                    Thread.sleep(1500);
                    break;
                }
                for(String event : events){
                    String[] args = event.split("__");
                    eventIds.add(Integer.parseInt(args[0]));
                    System.out.printf("Event id: %3s | %s | %2s:%2s %2s/%2s/%4s | %s", 
                                    args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
                    System.out.print("\n");
                    
                }
                
                System.out.print("\nInsert the id of the event you wish to update: ");
                input = Integer.parseInt(br.readLine());
                if(eventIds.contains(input)){
                    boolean uDescr = false;
                    boolean uTime = false;
                    boolean uDest = false;
                    String updateMsg = "" + input;
                    
                    System.out.print("\nDo You wish to update the description(y or n)? ");
                    String newDescr = br.readLine();
                    if(newDescr.equals("y")){//TO BE IMPLEMENTED...
                        System.out.print("\nInsert the new description: ");
                        newDescr = br.readLine();
                        updateMsg = updateMsg + "__" + newDescr;
                        uDescr = true;
                    }
                    else
                        updateMsg = updateMsg + "__noDescr";
                    
                    System.out.print("\nDo You wish to update the time(y or n)? ");
                    String newTime = br.readLine();
                    if(newTime.equals("y")){//TO BE IMPLEMENTED...
                        System.out.print("\nInsert the new time(hh mm dd MM yyyy): ");
                        newTime = br.readLine();
                        updateMsg = updateMsg + "__" + newTime.replace(" ", "__");
                        uTime = true;
                    }
                    else
                        updateMsg = updateMsg + "__" + 0 + "__" + 0 + "__" + 0 + "__" + 0 + "__" + 0;
                    
                    System.out.print("\nDo You wish to update the destination(y or n)? ");
                    String newDest = br.readLine();
                    if(newDest.equals("y")){
                        System.out.print("\nInsert the new destination: ");
                        newDest = br.readLine();
                        updateMsg = updateMsg + "__" + newDest;
                        uDest = true;
                    }
                    else
                        updateMsg = updateMsg + "__noDest";
                    
                    if(!uDescr && !uTime && !uDest){
                        //cls();
                        System.out.println("\n\nNo updates to be made!\n");
                        break;
                    }
                    
                    msg = context.createTextMessage(updateMsg);
                    msg.setStringProperty("from", "user");
                    msg.setStringProperty("to", "planner");
                    msg.setIntProperty("userid", myId);
                    msg.setStringProperty("op", "update");
                    msg.setBooleanProperty("uDescr", uDescr);
                    msg.setBooleanProperty("uTime", uTime);
                    msg.setBooleanProperty("uDest", uDest);
                    sender.send(plannerQ, msg);
                    
                    reply = consumer.receive();
                    System.out.println("\n\n" + reply.getBody(String.class) + "\n");
                }
                else{
                    //cls();
                    System.out.println("\n\nNo event with such id exists!\n");
                    Thread.sleep(1500);
                }
                break;
                
            case 4:
                //clr();
                msg = context.createTextMessage("");
                msg.setStringProperty("from", "user");
                msg.setStringProperty("to", "planner");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("op", "getAll");
                sender.send(plannerQ, msg);
                
                reply = consumer.receive();
                //clr();
                events = reply.getBody(String.class).split("\n");
                
                if(events[0].equals("-100")){
                    System.out.println("There are no events!");
                    Thread.sleep(1500);
                    break;
                }
                
                for(String event : events){
                    String[] args = event.split("__");

                    System.out.printf("Event id: %3s | %s | %2s:%2s %2s/%2s/%4s | %s", 
                                    args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
                    System.out.print("\n");
                }
                System.out.print("\nPress enter to continue...\n");
                br.readLine();
                break;
                
            case 5:
                System.out.print("\nInsert the target destination: ");
                String targetDest = br.readLine();
                System.out.println();
                System.out.print("\nInsert your starting location: ");//No current location
                String startingPoint = br.readLine();
                System.out.println();
                
                msg = context.createTextMessage(startingPoint + "__" + targetDest);
                msg.setStringProperty("from", "user");
                msg.setStringProperty("to", "planner");
                msg.setIntProperty("userid", myId);
                msg.setStringProperty("op", "dist");
                sender.send(plannerQ, msg);
                
                reply = consumer.receive();
                String[] travelTime = reply.getBody(String.class).split("__");
                System.out.printf("%s hours and %s minutes\n", travelTime[0], travelTime[1]);
                break;
            
            default:
                break;
        }
    }
    
    public static void main(String[] args) {
        UserDevice user = new UserDevice(1);//Integer.parseInt(args[0])
        
        try{
            user.context = user.cf.createContext();
            user.sender = user.context.createProducer();
            
            user.consumer = user.context.createConsumer(userQ, "userid=" + user.myId);
            /*user.flushAlarmQ = user.context.createConsumer(alarmQ);
            user.flushPlannerQ = user.context.createConsumer(plannerQ);
            user.flushSoundQ = user.context.createConsumer(soundQ);*/
            
            clr();
            System.out.println("Welcome " + user.myId + "!");
            Thread.sleep(1000);
            
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            
            while(!user.end){
                clr();
                System.out.println("1. Alarm\n2. Planner\n3. Sound device\n0. Exit");
                String input = br.readLine();
                
                if(input == null || input.equals(""))
                    continue;
                
                switch(Integer.parseInt(input)){
                    //Alarm
                    case 1:
                        user.alarmAction(br);
                        break;
                        
                    case 2:
                        user.plannerAction(br);
                        break;
                    
                    case 3:
                        user.soundAction(br);
                        break;
                    
                    case 8:
                        System.out.println(user.consumer.receiveNoWait());
                        /*System.out.println(user.flushAlarmQ.receiveNoWait());
                        System.out.println(user.flushPlannerQ.receiveNoWait());
                        System.out.println(user.flushSoundQ.receiveNoWait());*/
                        break;
                    
                    default:
                        user.end = true;
                        break;
                }
            }
        
        } catch (IOException ex) {
            Logger.getLogger(UserDevice.class.getName()).log(Level.SEVERE, null, ex);
        }   catch (InterruptedException ex) {
            Logger.getLogger(UserDevice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JMSException ex) {
            Logger.getLogger(UserDevice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
