/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.google.maps;

import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Filip Tanic
 */
public class Main {

    public static void main(String[] args) {
        try {
            String s = "";
            for (int i = 0; i < args.length; i++) {
                s += args[i];
            }
            String arr[] = s.split("/");
            TravelMode mode = null;
            if (args[2].equals("driving")) {
                mode = TravelMode.DRIVING;
            } else if (args[2].equals("walking")) {
                mode = TravelMode.WALKING;
            }
            GeoApiContext geo = new GeoApiContext.Builder().apiKey("AIzaSyBmAKU9tnUW8vvD4vCMbmklz6koxZG306A").build();
            DistanceMatrix await = DistanceMatrixApi.newRequest(geo)
                    .origins(args[0])
                    .destinations(args[1])
                    .mode(mode)
                    .language("hr")
                    .avoid(DirectionsApi.RouteRestriction.TOLLS)
                    .units(Unit.IMPERIAL)
                    .departureTime(
                            Instant.now().plus(Duration.ofMinutes(2))) // this is ignored when an API key is used
                    .await();
            com.google.maps.model.Duration duration = await.rows[0].elements[0].duration;
            System.out.println(duration.toString());
            geo.shutdown();
        } catch (ApiException | IOException | InterruptedException ex) {
            Logger.getLogger(DistanceMatrixApi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
